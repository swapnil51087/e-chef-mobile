package com.example.echef;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ActionBar;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.app.Activity;
import android.content.Intent;

public class MainActivity extends Activity {
	// Declare Tab Variable
	ActionBar.Tab Tab1, Tab2, Tab3;
	Intent i;	
	EditText tv;
	Search obj;
	Fragment fragmentTab1 = new PickFragment();
	Fragment fragmentTab2 = new SurpriseFragment();
	Fragment fragmentTab3 = new SearchFragment();
	static int currentViewId=-1;
	static boolean pickFragment=false;
	static boolean searchFragment=false;
	static boolean surpriseFragment=false;
	static String userPreferencesString;
	static String recommendationsString;
	static boolean recommendationFragment = false;
	static String surpriseString;
	static boolean isSignedIn = false;
	static String userId;
	static String search;
	static String imgSource = "https://s3-us-west-2.amazonaws.com/foodrecommendation.pictures/";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setCurrentViewById(R.id.fragment_container);
		obj = new Search();				
		createTabs();
		
		if(!surpriseFragment)
		{
			signInPostRequest(userId);
		}
	}
	
	public void createTabs()
	{
		ActionBar actionBar = getActionBar();

		// Hide Actionbar Icon
		actionBar.setDisplayShowHomeEnabled(true);

		// Hide Actionbar Title
		actionBar.setDisplayShowTitleEnabled(true);

		// Create Actionbar Tabs
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Set Tab Icon and Titles
		Tab1 = actionBar.newTab().setText("PICK");
		Tab2 = actionBar.newTab().setText("SURPRISE");
		Tab3 = actionBar.newTab().setText("SEARCH");


		// Set Tab Listeners
		Tab1.setTabListener(new TabListener(fragmentTab1));
		Tab2.setTabListener(new TabListener(fragmentTab2));
		Tab3.setTabListener(new TabListener(fragmentTab3));

		// Add tabs to actionbar
		actionBar.addTab(Tab1);
		actionBar.addTab(Tab2);
		actionBar.addTab(Tab3);
	}
	
	public void setCurrentViewById(int id)
    {       
        currentViewId = id;
    }
	  
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if(isSignedIn)
        inflater.inflate(R.menu.menu_signedin, menu);
        else
        	inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
	
	@Override
	  public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // action with ID action_refresh was selected
	    case R.id.aboutUs:
	    	i = new Intent(getApplicationContext(),AboutUs.class);
		      startActivity(i);
	      break;
	    // action with ID action_settings was selected
	    case R.id.userPreferences:
	    	i = new Intent(getApplicationContext(),UserPreferences.class);
		      startActivity(i);
	      break;
	      
	    case R.id.signOut:
	    	SignIn.isSignedIn  = false;
	    	  i = new Intent(getApplicationContext(),Main.class);
		      startActivity(i);
		      break;
		      
	    case R.id.signInorUp:
	    	
		      i = new Intent(getApplicationContext(),SignInOrSignUp.class);
		      startActivity(i);
		      break;
	    default:
	      break;
	    }

	    return true;
	  } 
	public void onSignUp(View view)
	{
		Intent i = new Intent(getApplicationContext(), SignUp.class);
		startActivity(i);
	}
	public void onSignIn(View view)
	{
		Intent i = new Intent(getApplicationContext(), SignIn.class);
		startActivity(i);	
	}
	
	private void signInPostRequest(String userId) {

	    class SignInPostReqAsyncTask extends AsyncTask<String, Void, String>{

	        @Override
	        protected String doInBackground(String... params) {

	           String userId = params[0];

	                try {
	                    
						HttpClient httpClient = new DefaultHttpClient();
			String url="http://54.215.152.32:8080/FoodRecommendationEngine/mobile/reco/"+userId;
		    // Prepare a request object
		    HttpGet httpget = new HttpGet(url); 
						// HttpResponse is an interface just like HttpPost.
	                    //Therefore we can't initialize them
						
						
	                    HttpResponse httpResponse = httpClient.execute(httpget);

	                    // According to the JAVA API, InputStream constructor do nothing. 
	                    //So we can't initialize InputStream although it is not an interface
	                    InputStream inputStream = httpResponse.getEntity().getContent();

	                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

	                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

	                    StringBuilder stringBuilder = new StringBuilder();

	                    String bufferedStrChunk = null;

	                    while((bufferedStrChunk = bufferedReader.readLine()) != null){
	                        stringBuilder.append(bufferedStrChunk);
	                    }

	                    return stringBuilder.toString();

	                } catch (ClientProtocolException cpe) {
	                    System.out.println("First Exception caz of HttpResponse :" + cpe);
	                    cpe.printStackTrace();
	                } catch (IOException ioe) {
	                    System.out.println("Second Exception caz of HttpResponse :" + ioe);
	                    ioe.printStackTrace();
	                }
					

	            return null;
	        }

	        @Override
	        protected void onPostExecute(String result) {
	            super.onPostExecute(result);
	            System.out.println("Recommendation String*** "+result);
	            if(result != null && !result.isEmpty())
	            {	     
	            		 MainActivity.surpriseString = result;
	            		 MainActivity.surpriseFragment = true;
	            		 //Toast.makeText(getActivity().getApplicationContext(), result, Toast.LENGTH_LONG).show(); 
	            		// Intent i= new Intent(getApplicationContext(),Recommendations.class);
	       		      //startActivity(i);
	            		 // getActivity().getActionBar().setSelectedNavigationItem(1);
	            }
	            else{
	                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
	            }
	        }           
	    }

	    SignInPostReqAsyncTask sendPostReqAsyncTask = new SignInPostReqAsyncTask();
	    sendPostReqAsyncTask.execute(userId);     
	}
	
	public void onSearchRestaurant(View view)
	{
		tv = (EditText) findViewById(R.id.tvRestaurant);
		obj.searchRestaurantPostRequest(tv.getText().toString());
	}
	
	public void onSearchFood(View view)
	{
	}
}

