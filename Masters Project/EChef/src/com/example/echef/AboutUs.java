package com.example.echef;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

@SuppressLint("NewApi") public class AboutUs extends Activity{
	Intent i;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.aboutus);

        TextView matilda = (TextView)findViewById(R.id.textView5);
        matilda.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Uri adress = Uri.parse("http://matildabernard.com");
                Intent browser = new Intent(Intent.ACTION_VIEW, adress);
                startActivity(browser);
            }

        });

        TextView swapnil = (TextView)findViewById(R.id.textView6);
        swapnil.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Uri adress = Uri.parse("https://linkedin.com/in/swapnilpancholi");
                Intent browser = new Intent(Intent.ACTION_VIEW, adress);
                startActivity(browser);
            }

        });

        TextView pranith = (TextView)findViewById(R.id.pranithText);
        pranith.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Uri adress = Uri.parse("https://linkedin.com/in/pranithp");
                Intent browser = new Intent(Intent.ACTION_VIEW, adress);
                startActivity(browser);
            }

        });
        TextView sarvagya = (TextView)findViewById(R.id.sarvagyaText);
        sarvagya.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Uri adress = Uri.parse("https://linkedin.com/in/sarvagyajain");
                Intent browser = new Intent(Intent.ACTION_VIEW, adress);
                startActivity(browser);
            }

        });
	}
	

	@Override
	 public boolean onPrepareOptionsMenu(Menu menu) {
		    return true;
		}

	 
	  
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if(SignIn.isSignedIn)
        {
        	inflater.inflate(R.menu.menu_signedin, menu);
        }
        else
        {
        	inflater.inflate(R.menu.main, menu);
        }
        
        return super.onCreateOptionsMenu(menu);
    }
	
	@Override
	  public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // action with ID action_refresh was selected
	    case R.id.aboutUs:
	    	i = new Intent(getApplicationContext(),AboutUs.class);
		      startActivity(i);
	      break;
	    // action with ID action_settings was selected
	    case R.id.userPreferences:
	    	i = new Intent(getApplicationContext(),UserPreferences.class);
		      startActivity(i);
	      break;
	      
	    case R.id.signOut:
	    	  SignIn.isSignedIn  = false;
	    	  i = new Intent(getApplicationContext(),Main.class);
		      startActivity(i);
		      break;
		      
	    case R.id.signInorUp:
		      i = new Intent(getApplicationContext(),SignInOrSignUp.class);
		      startActivity(i);
		      break;
	    default:
	      break;
	    }

	    return true;
	  }  


}
