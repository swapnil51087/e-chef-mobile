package com.example.echef;

import android.annotation.SuppressLint;
import android.app.ActionBar.Tab;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ActionBar;
import android.view.ViewGroup;
 
public class TabListener implements ActionBar.TabListener {
 
	Fragment fragment;
	public TabListener()
	{
		
	}
	@SuppressLint("NewApi") public TabListener(Fragment fragment) {
		// TODO Auto-generated constructor stub
		this.fragment = fragment;
	}
 
	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		//ft.replace(R.id.fragment_container, fragment);
		ft.replace(R.id.fragment_container,fragment);
	}
 
	@SuppressLint("NewApi") @Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		ft.remove(fragment);
	}
 
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
 
	}
}