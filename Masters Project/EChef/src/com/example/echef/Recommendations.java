package com.example.echef;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import com.example.echef.R.string;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;



@SuppressLint("NewApi") public class Recommendations extends Activity {
    Intent i;
    private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
    static int imageId = 1, buttonId = 1;
    static String userPreferencesString;
    JSONObject obj;
    String[] foodItemId = new String[0];
    /**
     * Called when the activity is first created.
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.recommendations);
        mainMethod();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore the previously serialized current tab position.
        if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
            getActionBar().setSelectedNavigationItem(savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // Serialize the current tab position.
        outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar()
                .getSelectedNavigationIndex());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (MainActivity.isSignedIn) {
            inflater.inflate(R.menu.menu_signedin, menu);
        } else {
            inflater.inflate(R.menu.main, menu);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.aboutUs:
                i = new Intent(getApplicationContext(), AboutUs.class);
                startActivity(i);
                break;
            // action with ID action_settings was selected
            case R.id.userPreferences:
                i = new Intent(getApplicationContext(), UserPreferences.class);
                startActivity(i);
                break;

            case R.id.signOut:
                SignIn.isSignedIn = false;
                i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                break;

            case R.id.signInorUp:
                i = new Intent(getApplicationContext(), SignInOrSignUp.class);
                startActivity(i);
                break;
            default:
                break;
        }

        return true;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    public void mainMethod() {
        userPreferencesString = null;
        if (MainActivity.recommendationFragment) {
            String[] itemname = new String[0];
            String[] description = new String[0];
            String[] imageUrl = new String[0];

            ListView list;

            try {

                JSONArray jObj = new JSONArray(MainActivity.recommendationsString);
                itemname = new String[jObj.length()];
                description = new String[jObj.length()];
                imageUrl = new String[jObj.length()];
                foodItemId = new String[jObj.length()];
                for (int i = 0; i < jObj.length(); i++) {
                    obj = jObj.getJSONObject(i);
                    itemname[i] = obj.getString("foodItemName");
                    description[i] = obj.getString("description");
                    imageUrl[i] = MainActivity.imgSource + obj.getString("foodItemId") + ".jpg";
                    foodItemId[i] = obj.getString("foodItemId");
                }
                System.out.println("Legnth of object");
                System.out.println(obj.length());

            } catch (Exception ex) {
            }
            CustomButtonListAdapter adapter = new CustomButtonListAdapter(this, itemname, description, imageUrl,foodItemId);
            list = (ListView)findViewById(R.id.recomListView);
            list.setAdapter(adapter);
            list.setItemsCanFocus(false);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // TODO Auto-generated method stub
                    try {
                        // Take to food item details page
                        updateLikesorDislikesPostRequest(foodItemId[position]);
                    } catch (Exception ex) {

                    }
                }
            });
        }

    }

    void updateLikesorDislikesPostRequest(String foodId) {
        System.out.println("Food Id:****"+foodId);
        class UpdateLikesorDislikesReqAsyncTask extends AsyncTask<String, Void, String>{

            @Override
            protected String doInBackground(String... params) {

                String foodId = params[0];
                try {

                    HttpClient httpClient = new DefaultHttpClient();
                    String url="http://54.215.152.32:8080/FoodRecommendationEngine/mobile/restaurant/fooditem/"+foodId;
                    System.out.println("URL****:"+url);
                    // Prepare a request object
                    HttpGet httpget = new HttpGet(url);
                    // HttpResponse is an interface just like HttpPost.
                    //Therefore we can't initialize them


                    HttpResponse httpResponse = httpClient.execute(httpget);

                    // According to the JAVA API, InputStream constructor do nothing.
                    //So we can't initialize InputStream although it is not an interface
                    InputStream inputStream = httpResponse.getEntity().getContent();

                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedStrChunk = null;

                    while((bufferedStrChunk = bufferedReader.readLine()) != null){
                        stringBuilder.append(bufferedStrChunk);
                    }
                    System.out.println("String Builder*** "+stringBuilder.toString());
                    return stringBuilder.toString();

                } catch (ClientProtocolException cpe) {
                    System.out.println("First Exception caz of HttpResponse :" + cpe);
                    cpe.printStackTrace();
                } catch (IOException ioe) {
                    System.out.println("Second Exception caz of HttpResponse :" + ioe);
                    ioe.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                System.out.println("Search result String*** "+result);
                if(result != null && !result.isEmpty())
                {
                    //Toast.makeText(getActivity().getApplicationContext(), result, Toast.LENGTH_LONG).show();
                    // getActivity().getActionBar().setSelectedNavigationItem(1);
                    MainActivity.search = result;
                    //Toast.makeText(getActivity().getApplicationContext(), result, Toast.LENGTH_LONG).show();
                    Intent i= new Intent(getApplicationContext(),SearchRestaurant.class);
                    i.putExtra("TitleText", "Restaurants where this food item can be found");
                    startActivity(i);
                }
                else{
                    //Toast.makeText(getActivity().getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                }
            }
        }

        UpdateLikesorDislikesReqAsyncTask sendPostReqAsyncTask = new UpdateLikesorDislikesReqAsyncTask();
        sendPostReqAsyncTask.execute(foodId);
    }
}


