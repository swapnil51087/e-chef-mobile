package com.example.echef;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi") public class SurpriseFragment extends Fragment{

    static int imageId = 1,buttonId=1;
    static String userPreferencesString;
    String[] foodItemId = new String[0];
    JSONObject obj;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.surprise, container, false);

        String[] itemname = new String[0];
        String[] description = new String[0];
        String[] imageUrl = new String[0];
        
        ListView list;

        userPreferencesString = null;
        if (MainActivity.surpriseFragment) {

            JSONArray jObj = null;
            try {
                jObj = new JSONArray(MainActivity.userPreferencesString);
                itemname = new String[jObj.length()];
                description = new String[jObj.length()];
                imageUrl = new String[jObj.length()];
                foodItemId = new String[jObj.length()];
                for (int i = 0; i < jObj.length(); i++) {
                    obj = jObj.getJSONObject(i);
                    itemname[i] = obj.getString("description");
                    description[i] = obj.getString("foodItemName");
                    imageUrl[i] = MainActivity.imgSource + obj.getString("foodItemId") + ".jpg";
                    foodItemId[i] = obj.getString("foodItemId");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            CustomButtonListAdapter adapter = new CustomButtonListAdapter(getActivity(), itemname, description, imageUrl,foodItemId);
            list = (ListView) rootView.findViewById(R.id.surpriseListView);
            list.setAdapter(adapter);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // TODO Auto-generated method stub
                    try {
                        // Take to food item details page
                    	updateLikesorDislikesPostRequest(foodItemId[position]);
                    } catch (Exception ex) {

                    }
                }
            });
        }
        return rootView;
    }
    
    void updateLikesorDislikesPostRequest(String foodId) {
    	System.out.println("Food Id:****"+foodId);
	    class UpdateLikesorDislikesReqAsyncTask extends AsyncTask<String, Void, String>{

	        @Override
	        protected String doInBackground(String... params) {

	           String foodId = params[0];
	                try {
	                    
						HttpClient httpClient = new DefaultHttpClient();
			String url="http://54.215.152.32:8080/FoodRecommendationEngine/mobile/restaurant/fooditem/"+foodId;
			System.out.println("URL****:"+url);
		    // Prepare a request object
		    HttpGet httpget = new HttpGet(url); 
						// HttpResponse is an interface just like HttpPost.
	                    //Therefore we can't initialize them
						
						
	                    HttpResponse httpResponse = httpClient.execute(httpget);

	                    // According to the JAVA API, InputStream constructor do nothing. 
	                    //So we can't initialize InputStream although it is not an interface
	                    InputStream inputStream = httpResponse.getEntity().getContent();

	                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

	                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

	                    StringBuilder stringBuilder = new StringBuilder();

	                    String bufferedStrChunk = null;

	                    while((bufferedStrChunk = bufferedReader.readLine()) != null){
	                        stringBuilder.append(bufferedStrChunk);
	                    }
	                    System.out.println("String Builder*** "+stringBuilder.toString());
	                    return stringBuilder.toString();

	                } catch (ClientProtocolException cpe) {
	                    System.out.println("First Exception caz of HttpResponse :" + cpe);
	                    cpe.printStackTrace();
	                } catch (IOException ioe) {
	                    System.out.println("Second Exception caz of HttpResponse :" + ioe);
	                    ioe.printStackTrace();
	                }
					

	            return null;
	        }

	        @Override
	        protected void onPostExecute(String result) {
	            super.onPostExecute(result);
	            System.out.println("Search result String*** "+result);
	            if(result != null && !result.isEmpty())
	            {	     	            		 
	            		 //Toast.makeText(getActivity().getApplicationContext(), result, Toast.LENGTH_LONG).show(); 	            		
	            		 // getActivity().getActionBar().setSelectedNavigationItem(1);
	            	MainActivity.search = result;
                    //Toast.makeText(getActivity().getApplicationContext(), result, Toast.LENGTH_LONG).show();
	            		 Intent i= new Intent(getActivity().getApplicationContext(),SearchRestaurant.class);
                        i.putExtra("TitleText", "Restaurants where this food item can be found");
	       		      startActivity(i);
	            }
	            else{
	                //Toast.makeText(getActivity().getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
	            }
	        }           
	    }

	    UpdateLikesorDislikesReqAsyncTask sendPostReqAsyncTask = new UpdateLikesorDislikesReqAsyncTask();
	    sendPostReqAsyncTask.execute(foodId);     
	}
    }
