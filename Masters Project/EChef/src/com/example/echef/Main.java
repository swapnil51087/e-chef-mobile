package com.example.echef;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Main extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
	}
    public void onViewStore(View view)
    {
        Intent intent = new Intent(getApplicationContext(), SignInOrSignUp.class);
        startActivity(intent);
    }



}
