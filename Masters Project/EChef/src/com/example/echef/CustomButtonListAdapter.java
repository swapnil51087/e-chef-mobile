package com.example.echef;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * Created by serah on 5/2/15.
 */
public class CustomButtonListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] itemnames;
    private final String[] descriptions;
    private final String[] imageUrls;
    private String[] foodItemId;
    private String foodId;
    private static int i=0;
    public CustomButtonListAdapter(Activity context, String[] itemNames, String[] descriptions, String[] imageUrls,String[] foodItemId){
        super(context, R.layout.list_item, itemNames);

        this.context=context;
        this.itemnames=itemNames;
        this.descriptions=descriptions;
        this.imageUrls = imageUrls;
        this.foodItemId = foodItemId;
    }


    public View getView(final int position,View view,ViewGroup parent) {
    	
    	
    	foodId = foodItemId[position];
    	System.out.println("i*****"+i);
    	System.out.println("foodId*****"+foodId);
        LayoutInflater inflater=context.getLayoutInflater();
        final View rowView=inflater.inflate(R.layout.button_list_item, null, true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.title1);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon1);
        TextView desc = (TextView) rowView.findViewById(R.id.desc1);
        Button likeButton = (Button) rowView.findViewById(R.id.likeButton);
        Button dislikeButton = (Button) rowView.findViewById(R.id.dislikeButton);
        likeButton.setFocusable(false);
        likeButton.setFocusableInTouchMode(false);
        likeButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                //Toast.makeText(getContext(), "Like Clicked"+foodItemId[position], Toast.LENGTH_LONG).show();
            	
                updateLikesorDislikesPostRequest("like",MainActivity.userId,foodItemId[position]);
            }
        });
        dislikeButton.setFocusable(false);
        dislikeButton.setFocusableInTouchMode(false);
        dislikeButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                //Toast.makeText(getContext(), "Dislike Clicked", Toast.LENGTH_LONG).show();
                updateLikesorDislikesPostRequest("dislike",MainActivity.userId,foodItemId[position]);
            }
        });


        txtTitle.setText(itemnames[position]);
        desc.setText(descriptions[position]);

        new DownloadImageTask(imageView).execute(imageUrls[position]);

        return rowView;
    };
    
    void updateLikesorDislikesPostRequest(String operation,String userId, String foodId) {

	    class UpdateLikesorDislikesReqAsyncTask extends AsyncTask<String, Void, String>{

	        @Override
	        protected String doInBackground(String... params) {

	           String operation = params[0];
	           String userId = params[1];
	           String foodId = params[2];
	                try {
	                    
						HttpClient httpClient = new DefaultHttpClient();
			String url="http://54.215.152.32:8080/FoodRecommendationEngine/mobile/prefs/"+operation+"/"+userId+"/"+foodId;
			System.out.println("URL****:"+url);
		    // Prepare a request object
		    HttpGet httpget = new HttpGet(url); 
						// HttpResponse is an interface just like HttpPost.
	                    //Therefore we can't initialize them
						
						
	                    HttpResponse httpResponse = httpClient.execute(httpget);

	                    // According to the JAVA API, InputStream constructor do nothing. 
	                    //So we can't initialize InputStream although it is not an interface
	                    InputStream inputStream = httpResponse.getEntity().getContent();

	                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

	                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

	                    StringBuilder stringBuilder = new StringBuilder();

	                    String bufferedStrChunk = null;

	                    while((bufferedStrChunk = bufferedReader.readLine()) != null){
	                        stringBuilder.append(bufferedStrChunk);
	                    }

	                    return stringBuilder.toString();

	                } catch (ClientProtocolException cpe) {
	                    System.out.println("First Exception caz of HttpResponse :" + cpe);
	                    cpe.printStackTrace();
	                } catch (IOException ioe) {
	                    System.out.println("Second Exception caz of HttpResponse :" + ioe);
	                    ioe.printStackTrace();
	                }
					

	            return null;
	        }

	        @Override
	        protected void onPostExecute(String result) {
	            super.onPostExecute(result);
	            System.out.println("Search result String*** "+result);
	            if(result != null && !result.isEmpty())
	            {	     	            		 
	            		 //Toast.makeText(getActivity().getApplicationContext(), result, Toast.LENGTH_LONG).show(); 	            		
	            		 // getActivity().getActionBar().setSelectedNavigationItem(1);
	            }
	            else{
	                //Toast.makeText(getActivity().getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
	            }
	        }           
	    }

	    UpdateLikesorDislikesReqAsyncTask sendPostReqAsyncTask = new UpdateLikesorDislikesReqAsyncTask();
	    sendPostReqAsyncTask.execute(operation,userId,foodId);     
	}
}
