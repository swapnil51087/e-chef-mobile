package com.example.echef;

import java.io.InputStream;

import org.json.JSONArray;
import org.json.JSONObject;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class SearchFood extends Activity{
    static int imageId = 1,buttonId=1;
    String titleStr;
    JSONObject obj;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.searchresult);
        this.titleStr = getIntent().getStringExtra("TitleText");
        mainMethod();
    }

    public void mainMethod()
    {
        String[] itemname = new String[0];
        String[] description = new String[0];
        String[] imageUrl = new String[0];
        ListView list;

        try
        {

            JSONArray jObj = new JSONArray(MainActivity.search);
            itemname = new String[jObj.length()];
            description = new String[jObj.length()];
            imageUrl = new String[jObj.length()];

            for(int i=0;i<jObj.length();i++) {
                obj = jObj.getJSONObject(i);
                itemname[i] = obj.getString("foodItemName");
                description[i] = obj.getString("description");
                imageUrl[i] = MainActivity.imgSource + obj.getString("foodItemId") + ".jpg";
            }

        }
        catch(Exception ex)
        {}

        if (titleStr != null){
        TextView txtTitle = (TextView)findViewById(R.id.searchResultText);
        txtTitle.setText(titleStr);}

        CustomListAdapter adapter=new CustomListAdapter(this, itemname, description, imageUrl);
        list=(ListView)findViewById(R.id.searchListView);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                try{
                    // Add onclick to go to view food details page
                }
                catch(Exception ex)
                {

                }
            }
        });
    }
}
