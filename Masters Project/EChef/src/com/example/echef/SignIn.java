package com.example.echef;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import com.example.echef.R.string;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;



@SuppressLint("NewApi") public class SignIn extends Activity implements
ActionBar.TabListener {
	Intent i;
	private EditText etEmail, etPassword,etFirstName,etLastName,etConfirmPassword;
	static boolean isSignedIn;
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.signin);
		isSignedIn = false;		
	}
	@Override
	 public boolean onPrepareOptionsMenu(Menu menu) {

		    return true;
		}
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        
        
        return super.onCreateOptionsMenu(menu);
    }
	
	@Override
	  public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // action with ID action_refresh was selected
	    case R.id.aboutUs:
	    	i = new Intent(getApplicationContext(),AboutUs.class);
		      startActivity(i);
	      break;
	    // action with ID action_settings was selected
	    case R.id.userPreferences:
	    	i = new Intent(getApplicationContext(),UserPreferences.class);
		      startActivity(i);
	      break;
	      
	    case R.id.signOut:
	    	MainActivity.isSignedIn  = false;
	    	  i = new Intent(getApplicationContext(),MainActivity.class);
		      startActivity(i);
		      break;
		      
	    case R.id.signInorUp:
	    	Toast.makeText(this, "Sign Up selected", Toast.LENGTH_SHORT)
	          .show();
		      i = new Intent(getApplicationContext(),SignInOrSignUp.class);
		      startActivity(i);
		      break;
	    default:
	      break;
	    }

	    return true;
	  } 

	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
        
		Intent i;
		if(arg0.getText().equals( getString(string.whatKindLabel)))
		{
			i = new Intent(getApplicationContext(),MainActivity.class);
			startActivity(i);			
		}
		else if(arg0.getText().equals(getString(string.recommendation_label)))
		{
			i = new Intent(getApplicationContext(),Recommendations.class);
			startActivity(i);
		}
		else if( arg0.getText().equals(getString(string.searchLabel) ))
		{
			i = new Intent(getApplicationContext(),Recommendations.class);
			startActivity(i);			
		}
	}
	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}	
	public void onSubmit(View view)
	{
		etEmail = (EditText) findViewById(R.id.etEmail);
		etPassword = (EditText) findViewById(R.id.etPassword);
		
		
	/*	if(!validEmail(etEmail.getText().toString()))
		{
			etEmail.setError( "Enter a valid email!" );
		}*/
		if( etEmail.getText().toString().trim().equals(""))
		 {    
			etEmail.setError( "Email is required!" );
			//Toast.makeText(getApplicationContext(), "Email cannot be empty", Toast.LENGTH_SHORT).show();		   
		 }
		 else if( etPassword.getText().toString().trim().equals(""))
		 {
			 etPassword.setError( "Password is required!" );
				//Toast.makeText(getApplicationContext(), "Password cannot be empty", Toast.LENGTH_SHORT).show();
		 }
		else
		{
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
    		SharedPreferences.Editor editor = preferences.edit();
    		editor.putString("userId",etEmail.getText().toString());
    		editor.apply();
    		MainActivity.userId = etEmail.getText().toString();
			signInPostRequest(etEmail.getText().toString(),etPassword.getText().toString());			
		}						
	}
	private boolean validEmail(String email) {
	    Pattern pattern = Patterns.EMAIL_ADDRESS;
	    return pattern.matcher(email).matches();
	}
	
	private void signInPostRequest(String email, String password) {

	    class SignInPostReqAsyncTask extends AsyncTask<String, Void, String>{

	        @Override
	        protected String doInBackground(String... params) {

	            String paramUsername = params[0];
	            String paramPassword = params[1];

	            System.out.println("*** doInBackground ** paramUsername " + paramUsername + " paramPassword :" + paramPassword);

	            HttpClient httpClient = new DefaultHttpClient();

	            // In a POST request, we don't pass the values in the URL.
	            //Therefore we use only the web page URL as the parameter of the HttpPost argument
	            HttpPost httpPost = new HttpPost("http://54.215.152.32:8080/FoodRecommendationEngine/mobile/signin");
	            httpPost.setHeader(HTTP.CONTENT_TYPE,
		                "application/x-www-form-urlencoded;charset=UTF-8");
	            // Because we are not passing values over the URL, we should have a mechanism to pass the values that can be
	            //uniquely separate by the other end.
	            //To achieve that we use BasicNameValuePair             
	            //Things we need to pass with the POST request
	            BasicNameValuePair usernameBasicNameValuePair = new BasicNameValuePair("userid", paramUsername);
	            BasicNameValuePair passwordBasicNameValuePAir = new BasicNameValuePair("password", paramPassword);

	            // We add the content that we want to pass with the POST request to as name-value pairs
	            //Now we put those sending details to an ArrayList with type safe of NameValuePair
	            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
	            nameValuePairList.add(usernameBasicNameValuePair);
	            nameValuePairList.add(passwordBasicNameValuePAir);

	            try {
	                // UrlEncodedFormEntity is an entity composed of a list of url-encoded pairs. 
	                //This is typically useful while sending an HTTP POST request. 
	                UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);

	                // setEntity() hands the entity (here it is urlEncodedFormEntity) to the request.
	                httpPost.setEntity(urlEncodedFormEntity);

	                try {
	                    // HttpResponse is an interface just like HttpPost.
	                    //Therefore we can't initialize them
	                    HttpResponse httpResponse = httpClient.execute(httpPost);

	                    // According to the JAVA API, InputStream constructor do nothing. 
	                    //So we can't initialize InputStream although it is not an interface
	                    InputStream inputStream = httpResponse.getEntity().getContent();

	                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

	                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

	                    StringBuilder stringBuilder = new StringBuilder();

	                    String bufferedStrChunk = null;

	                    while((bufferedStrChunk = bufferedReader.readLine()) != null){
	                        stringBuilder.append(bufferedStrChunk);
	                    }

	                    return stringBuilder.toString();

	                } catch (ClientProtocolException cpe) {
	                    System.out.println("First Exception caz of HttpResponese :" + cpe);
	                    cpe.printStackTrace();
	                } catch (IOException ioe) {
	                    System.out.println("Second Exception caz of HttpResponse :" + ioe);
	                    ioe.printStackTrace();
	                }

	            } catch (UnsupportedEncodingException uee) {
	                System.out.println("An Exception given because of UrlEncodedFormEntity argument :" + uee);
	                uee.printStackTrace();
	            }

	            return null;
	        }

	        @Override
	        protected void onPostExecute(String result) {
	            super.onPostExecute(result);
	            System.out.println("REsult*** "+result);
	            if(result != null && !result.isEmpty())
	            {	            		            
	            	try {
	            		JSONObject obj = new JSONObject(result);
	            		System.out.println("JSON userid:"+obj.getString("userId"));	            		
	            		MainActivity.isSignedIn = true;
	            		Toast.makeText(getApplicationContext(), "Welcome,"+obj.getString("firstName"), Toast.LENGTH_LONG).show();
	            		
	            		
	            		
	            		Intent i = new Intent(getApplicationContext(),UserPreferences.class);
		        		startActivity(i);		                
	            	} catch (Throwable t) {
	                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
	            	}	            	
	            }
	            else{
	                Toast.makeText(getApplicationContext(), "Invalid credentials", Toast.LENGTH_LONG).show();
	            }
	        }           
	    }

	    SignInPostReqAsyncTask sendPostReqAsyncTask = new SignInPostReqAsyncTask();
	    sendPostReqAsyncTask.execute(email, password);     
	}
}
