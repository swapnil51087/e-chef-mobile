package com.example.echef;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
@SuppressLint("NewApi") public class PickFragment extends Fragment{

    static int imageId = 1;
    static String userPreferencesString;
    String[] foodItemId = new String[0];
    JSONObject obj;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.pick, container, false);
        String[] itemname = new String[0];
        String[] description = new String[0];
        String[] imageUrl = new String[0];
        
        ListView list;

        userPreferencesString=null;
        if(MainActivity.pickFragment)
        {
            JSONArray jObj = null;
            try {
                jObj = new JSONArray(MainActivity.userPreferencesString);
                itemname = new String[jObj.length()];
                description = new String[jObj.length()];
                imageUrl = new String[jObj.length()];
                foodItemId = new String[jObj.length()];
                for(int i=0;i<jObj.length();i++) {
                    obj = jObj.getJSONObject(i);
                    itemname[i] = obj.getString("description");
                    description[i] = obj.getString("foodItemName");
                    imageUrl[i] = MainActivity.imgSource + obj.getString("foodItemId") + ".jpg";
                    foodItemId[i] = obj.getString("foodItemId");
                    System.out.println("Image URL****:"+imageUrl[i]);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            CustomListAdapter adapter=new CustomListAdapter(getActivity(), itemname, description, imageUrl);
            list=(ListView)rootView.findViewById(R.id.listView);
            list.setAdapter(adapter);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // TODO Auto-generated method stub
                    try{
                        signInPostRequest(foodItemId[position]);
                    }
                    catch(Exception ex)
                    {

                    }
                }
            });
        }
    return rootView;
}
    private void signInPostRequest(String foodItemId) {

        class SignInPostReqAsyncTask extends AsyncTask<String, Void, String>{

            @Override
            protected String doInBackground(String... params) {

                String foodItemId = params[0];

                try {

                    HttpClient httpClient = new DefaultHttpClient();
                    String url="http://54.215.152.32:8080/FoodRecommendationEngine/mobile/related/"+foodItemId;
                    // Prepare a request object
                    HttpGet httpget = new HttpGet(url);
                    // HttpResponse is an interface just like HttpPost.
                    //Therefore we can't initialize them


                    HttpResponse httpResponse = httpClient.execute(httpget);

                    // According to the JAVA API, InputStream constructor do nothing.
                    //So we can't initialize InputStream although it is not an interface
                    InputStream inputStream = httpResponse.getEntity().getContent();

                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedStrChunk = null;

                    while((bufferedStrChunk = bufferedReader.readLine()) != null){
                        stringBuilder.append(bufferedStrChunk);
                    }

                    return stringBuilder.toString();

                } catch (ClientProtocolException cpe) {
                    System.out.println("First Exception caz of HttpResponse :" + cpe);
                    cpe.printStackTrace();
                } catch (IOException ioe) {
                    System.out.println("Second Exception caz of HttpResponse :" + ioe);
                    ioe.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                System.out.println("Recommendation String*** "+result);
                if(result != null && !result.isEmpty())
                {
                    MainActivity.recommendationsString = result;
                    MainActivity.recommendationFragment = true;
                    //Toast.makeText(getActivity().getApplicationContext(), result, Toast.LENGTH_LONG).show();
                    Intent i= new Intent(getActivity().getApplicationContext(),Recommendations.class);
                    startActivity(i);
                    // getActivity().getActionBar().setSelectedNavigationItem(1);
                }
                else{
                    Toast.makeText(getActivity().getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                }
            }
        }

        SignInPostReqAsyncTask sendPostReqAsyncTask = new SignInPostReqAsyncTask();
        sendPostReqAsyncTask.execute(foodItemId);
    }

}


