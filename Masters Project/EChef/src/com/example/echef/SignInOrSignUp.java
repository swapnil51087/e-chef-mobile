package com.example.echef;



import com.example.echef.R.string;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;



@SuppressLint("NewApi") public class SignInOrSignUp extends Activity  {
	Intent i;
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.signinorsignup);
	}
	@Override
	 public boolean onPrepareOptionsMenu(Menu menu) {

		    return true;
		}
		  
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        
        
        return super.onCreateOptionsMenu(menu);
    }
	
	@Override
	  public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // action with ID action_refresh was selected
	    case R.id.aboutUs:
	    	i = new Intent(getApplicationContext(),AboutUs.class);
		      startActivity(i);
	      break;
	    // action with ID action_settings was selected
	    case R.id.userPreferences:
	    	i = new Intent(getApplicationContext(),UserPreferences.class);
		      startActivity(i);
	      break;
	      
	    case R.id.signOut:
	    	SignIn.isSignedIn  = false;
	    	  i = new Intent(getApplicationContext(),MainActivity.class);
		      startActivity(i);
		      break;
		      
	    case R.id.signInorUp:
	    	
		      i = new Intent(getApplicationContext(),SignInOrSignUp.class);
		      startActivity(i);
		      break;
	    default:
	      break;
	    }

	    return true;
	  } 
	public void onSignUp(View view)
	{
		Intent i = new Intent(getApplicationContext(), SignUp.class);
		startActivity(i);
	}
	public void onSignIn(View view)
	{
		Intent i = new Intent(getApplicationContext(), SignIn.class);
		startActivity(i);	
	}

	}
