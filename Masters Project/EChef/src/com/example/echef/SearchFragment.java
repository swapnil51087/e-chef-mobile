package com.example.echef;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

@SuppressLint("NewApi") public class SearchFragment extends Fragment{
	EditText tv;
	Search obj;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.search, container, false);
        
        Button sin =  (Button) rootView.findViewById(R.id.btnFood);
        sin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
            	obj = new Search();
                Toast.makeText(getActivity().getApplicationContext(), "Test", Toast.LENGTH_LONG).show();
                tv = (EditText) getActivity().findViewById(R.id.tvFoodItem);
        		searchFoodPostRequest(tv.getText().toString());

            }
        });
        
        Button btn2 =  (Button) rootView.findViewById(R.id.btnRestaurant);
        btn2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
            	obj = new Search();
                Toast.makeText(getActivity().getApplicationContext(), "Test", Toast.LENGTH_LONG).show();
                tv = (EditText) getActivity().findViewById(R.id.tvRestaurant);
        		searchRestaurantPostRequest(tv.getText().toString());

            }
        });
        return rootView;
    }
	

	void searchRestaurantPostRequest(String input) {

	    class SignInPostReqAsyncTask extends AsyncTask<String, Void, String>{

	        @Override
	        protected String doInBackground(String... params) {

	           String input = params[0];

	                try {
	                    
						HttpClient httpClient = new DefaultHttpClient();
			String url="http://54.215.152.32:8080/FoodRecommendationEngine/mobile/search/restaurant/"+input;
			System.out.println("URL****:"+url);
		    // Prepare a request object
		    HttpGet httpget = new HttpGet(url); 
						// HttpResponse is an interface just like HttpPost.
	                    //Therefore we can't initialize them
						
						
	                    HttpResponse httpResponse = httpClient.execute(httpget);

	                    // According to the JAVA API, InputStream constructor do nothing. 
	                    //So we can't initialize InputStream although it is not an interface
	                    InputStream inputStream = httpResponse.getEntity().getContent();

	                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

	                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

	                    StringBuilder stringBuilder = new StringBuilder();

	                    String bufferedStrChunk = null;

	                    while((bufferedStrChunk = bufferedReader.readLine()) != null){
	                        stringBuilder.append(bufferedStrChunk);
	                    }

	                    return stringBuilder.toString();

	                } catch (ClientProtocolException cpe) {
	                    System.out.println("First Exception caz of HttpResponse :" + cpe);
	                    cpe.printStackTrace();
	                } catch (IOException ioe) {
	                    System.out.println("Second Exception caz of HttpResponse :" + ioe);
	                    ioe.printStackTrace();
	                }
					

	            return null;
	        }

	        @Override
	        protected void onPostExecute(String result) {
	            super.onPostExecute(result);
	            System.out.println("Search result String*** "+result);
	            if(result != null && !result.isEmpty())
	            {	     
	            		 MainActivity.search = result;	            		 
	            		 //Toast.makeText(getActivity().getApplicationContext(), result, Toast.LENGTH_LONG).show(); 
	            		 Intent i= new Intent(getActivity().getApplicationContext(),SearchRestaurant.class);
	       		      startActivity(i);
	            		 // getActivity().getActionBar().setSelectedNavigationItem(1);
	            }
	            else{
	                Toast.makeText(getActivity().getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
	            }
	        }           
	    }

	    SignInPostReqAsyncTask sendPostReqAsyncTask = new SignInPostReqAsyncTask();
	    sendPostReqAsyncTask.execute(input);     
	}
	
	void searchFoodPostRequest(String input) {

	    class SignInPostReqAsyncTask extends AsyncTask<String, Void, String>{

	        @Override
	        protected String doInBackground(String... params) {

	           String input = params[0];

	                try {
	                    
						HttpClient httpClient = new DefaultHttpClient();
			String url="http://54.215.152.32:8080/FoodRecommendationEngine/mobile/search/fooditem/"+input;
		    // Prepare a request object
		    HttpGet httpget = new HttpGet(url); 
						// HttpResponse is an interface just like HttpPost.
	                    //Therefore we can't initialize them
						
						
	                    HttpResponse httpResponse = httpClient.execute(httpget);

	                    // According to the JAVA API, InputStream constructor do nothing. 
	                    //So we can't initialize InputStream although it is not an interface
	                    InputStream inputStream = httpResponse.getEntity().getContent();

	                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

	                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

	                    StringBuilder stringBuilder = new StringBuilder();

	                    String bufferedStrChunk = null;

	                    while((bufferedStrChunk = bufferedReader.readLine()) != null){
	                        stringBuilder.append(bufferedStrChunk);
	                    }

	                    return stringBuilder.toString();

	                } catch (ClientProtocolException cpe) {
	                    System.out.println("First Exception caz of HttpResponse :" + cpe);
	                    cpe.printStackTrace();
	                } catch (IOException ioe) {
	                    System.out.println("Second Exception caz of HttpResponse :" + ioe);
	                    ioe.printStackTrace();
	                }
					

	            return null;
	        }

	        @Override
	        protected void onPostExecute(String result) {
	            super.onPostExecute(result);
	            System.out.println("Search result String*** "+result);
	            if(result != null && !result.isEmpty())
	            {	     
	            		 MainActivity.search = result;	            		 
	            		 //Toast.makeText(getActivity().getApplicationContext(), result, Toast.LENGTH_LONG).show(); 
	            		 Intent i= new Intent(getActivity().getApplicationContext(),SearchFood.class);
	       		      startActivity(i);
	            		 // getActivity().getActionBar().setSelectedNavigationItem(1);
	            }
	            else{
	                Toast.makeText(getActivity().getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
	            }
	        }           
	    }

	    SignInPostReqAsyncTask sendPostReqAsyncTask = new SignInPostReqAsyncTask();
	    sendPostReqAsyncTask.execute(input);     
	}
}
