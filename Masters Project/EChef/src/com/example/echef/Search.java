package com.example.echef;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Search extends Activity{
    EditText tv;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);
    }




    void searchRestaurantPostRequest(String input) {

        class SignInPostReqAsyncTask extends AsyncTask<String, Void, String>{

            @Override
            protected String doInBackground(String... params) {

                String input = params[0];

                try {

                    HttpClient httpClient = new DefaultHttpClient();
                    String url="http://54.215.152.32:8080/FoodRecommendationEngine/mobile/search/restaurant/"+input;
                    System.out.println("URL****:"+url);
                    // Prepare a request object
                    HttpGet httpget = new HttpGet(url);
                    // HttpResponse is an interface just like HttpPost.
                    //Therefore we can't initialize them


                    HttpResponse httpResponse = httpClient.execute(httpget);

                    // According to the JAVA API, InputStream constructor do nothing.
                    //So we can't initialize InputStream although it is not an interface
                    InputStream inputStream = httpResponse.getEntity().getContent();

                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedStrChunk = null;

                    while((bufferedStrChunk = bufferedReader.readLine()) != null){
                        stringBuilder.append(bufferedStrChunk);
                    }

                    return stringBuilder.toString();

                } catch (ClientProtocolException cpe) {
                    System.out.println("First Exception caz of HttpResponse :" + cpe);
                    cpe.printStackTrace();
                } catch (IOException ioe) {
                    System.out.println("Second Exception caz of HttpResponse :" + ioe);
                    ioe.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                System.out.println("Search result String*** "+result);
                if(result != null && !result.isEmpty())
                {
                    MainActivity.search = result;
                    //Toast.makeText(getActivity().getApplicationContext(), result, Toast.LENGTH_LONG).show();
                    Intent i= new Intent(getApplicationContext(),SearchRestaurant.class);
                    i.putExtra("TitleText", "Matching restaurants");
                    startActivity(i);
                    // getActivity().getActionBar().setSelectedNavigationItem(1);
                }
                else{
                    Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                }
            }
        }

        SignInPostReqAsyncTask sendPostReqAsyncTask = new SignInPostReqAsyncTask();
        sendPostReqAsyncTask.execute(input);
    }

    void searchFoodPostRequest(String input) {

        class SignInPostReqAsyncTask extends AsyncTask<String, Void, String>{

            @Override
            protected String doInBackground(String... params) {

                String input = params[0];

                try {

                    HttpClient httpClient = new DefaultHttpClient();
                    String url="http://54.215.152.32:8080/FoodRecommendationEngine/mobile/search/fooditem/"+input;
                    // Prepare a request object
                    HttpGet httpget = new HttpGet(url);
                    // HttpResponse is an interface just like HttpPost.
                    //Therefore we can't initialize them


                    HttpResponse httpResponse = httpClient.execute(httpget);

                    // According to the JAVA API, InputStream constructor do nothing.
                    //So we can't initialize InputStream although it is not an interface
                    InputStream inputStream = httpResponse.getEntity().getContent();

                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedStrChunk = null;

                    while((bufferedStrChunk = bufferedReader.readLine()) != null){
                        stringBuilder.append(bufferedStrChunk);
                    }

                    return stringBuilder.toString();

                } catch (ClientProtocolException cpe) {
                    System.out.println("First Exception caz of HttpResponse :" + cpe);
                    cpe.printStackTrace();
                } catch (IOException ioe) {
                    System.out.println("Second Exception caz of HttpResponse :" + ioe);
                    ioe.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                System.out.println("Search result String*** "+result);
                if(result != null && !result.isEmpty())
                {
                    MainActivity.search = result;
                    //Toast.makeText(getActivity().getApplicationContext(), result, Toast.LENGTH_LONG).show();
                    Intent i= new Intent(getApplicationContext(),SearchFood.class);
                    i.putExtra("TitleText", "Matching food items");
                    startActivity(i);
                    // getActivity().getActionBar().setSelectedNavigationItem(1);
                }
                else{
                    Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                }
            }
        }

        SignInPostReqAsyncTask sendPostReqAsyncTask = new SignInPostReqAsyncTask();
        sendPostReqAsyncTask.execute(input);
    }
}
