package com.example.echef;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.os.AsyncTask;
import android.util.Log;


class UserController extends AsyncTask<String,Void,Void> {

    private Exception exception;

    protected Void doInBackground(String... urls) {
        //checkSignin();
		HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost("http://54.215.152.32:8080/FoodRecommendationEngine/mobile/signin");
	    httppost.setHeader(HTTP.CONTENT_TYPE,
                "application/x-www-form-urlencoded;charset=UTF-8");
	    try {
	        // Add your data
	    	
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
	        nameValuePairs.add(new BasicNameValuePair("userid", "s@s"));
	        nameValuePairs.add(new BasicNameValuePair("password", "s"));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
	        StringBuilder builder = new StringBuilder();
	        for (String line = null; (line = reader.readLine()) != null;) {
	            builder.append(line).append("\n");
	        }
	        JSONTokener tokener = new JSONTokener(builder.toString());
	        JSONArray finalResult = new JSONArray(tokener);	 
	        
	        System.out.println("Helloo");
	        for(int i=0;i<finalResult.length();i++)
	        {
	        	JSONObject jsonobject= (JSONObject) finalResult.get(i);
	        	  String id=jsonobject.optString("userId");
	        	 Log.d("userId", id);
	        	 System.out.println("ID"+id);
	        }
	        //Toast.makeText(getApplicationContext(), json_string, Toast.LENGTH_LONG).show();
	       
	    } catch (Exception e) {
            this.exception = e;
            return null;
        }
	    return null;
    }

    protected void onPostExecute() {
        // TODO: check this.exception 
        // TODO: do something with the feed
    }
}