package com.example.echef;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import com.example.echef.R.string;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.CheckBox;


@SuppressLint("NewApi") public class UserPreferences extends Activity{
	Intent i;
	private List<String> cuisines;
	private List<String> allergies;
	private String category;
	private String userId;
	private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";	
	Spinner spinnerCategory;
	static String userPreferencesString=null;
	CheckBox cb;
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.userpreferences);
		cuisines = new ArrayList<String>();	
		allergies = new ArrayList<String>();
		
		
	}
	public void onCheckboxClicked(View view)
	{
		
	    boolean checked = ((CheckBox) view).isChecked();
	    
	    
	    switch(view.getId()) {
	        case R.id.cb_indianCuisine:
	            if (checked){
	            	cuisines.add("Indian");
	            }	        
	            break;
	        case R.id.cb_thaiCuisine:
	            if (checked){
	            	cuisines.add("Thai");
	            }
	        
	            else{}
	        
	            break;
	        
	    }
	}
	
	@Override
	 public boolean onPrepareOptionsMenu(Menu menu) {

		    return true;
		}
	@Override
	  public void onRestoreInstanceState(Bundle savedInstanceState) {
	    // Restore the previously serialized current tab position.
	    if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
	      getActionBar().setSelectedNavigationItem(savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
	    }
	  }

	  @Override
	  public void onSaveInstanceState(Bundle outState) {
	    // Serialize the current tab position.
	    outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar()
	        .getSelectedNavigationIndex());
	  }
	  
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_signedin, menu);
        
        
        return super.onCreateOptionsMenu(menu);
    }
	
	@Override
	  public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // action with ID action_refresh was selected
	    case R.id.aboutUs:
	    	i = new Intent(getApplicationContext(),AboutUs.class);
		      startActivity(i);
	      break;
	    // action with ID action_settings was selected
	    case R.id.userPreferences:
	    	i = new Intent(getApplicationContext(),UserPreferences.class);
		      startActivity(i);
	      break;
	      
	    case R.id.signOut:
	    	SignIn.isSignedIn  = false;
	    	  i = new Intent(getApplicationContext(),Main.class);
		      startActivity(i);
		      break;
		      
	    case R.id.signInorUp:
	    	
		      i = new Intent(getApplicationContext(),SignInOrSignUp.class);
		      startActivity(i);
		      break;
	    default:
	      break;
	    }

	    return true;
	  } 
	
	public void onSubmit(View view)
	{
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		userId = preferences.getString("userId", "");		
		
		spinnerCategory=(Spinner) findViewById(R.id.userType);				
		category = getResources().getStringArray(R.array.user_type_val)[spinnerCategory.getSelectedItemPosition()];
		
		cb = (CheckBox) findViewById(R.id.cb_indianCuisine);
		if(cb.isChecked())
		{
			cuisines.add("Indian");
		}
		
		cb = (CheckBox) findViewById(R.id.cb_thaiCuisine);
		if(cb.isChecked())
		{
			cuisines.add("Thai");
		}
		
		cb=(CheckBox) findViewById(R.id.cb_eggs);
		if(cb.isChecked())
		{
			allergies.add("EGG");
		}
		
		cb=(CheckBox) findViewById(R.id.cb_fish);
		if(cb.isChecked())
		{
			allergies.add("FISH");
		}
		
		cb=(CheckBox) findViewById(R.id.cb_milk);
		if(cb.isChecked())
		{
			allergies.add("MILK");
		}
		
		cb=(CheckBox) findViewById(R.id.cb_nuts);
		if(cb.isChecked())
		{
			allergies.add("NUT");
		}
		
		cb=(CheckBox) findViewById(R.id.cb_peanuts);
		if(cb.isChecked())
		{
			allergies.add("PEANUT");
		}
		
		cb=(CheckBox) findViewById(R.id.cb_soya);
		if(cb.isChecked())
		{
			allergies.add("SOYA");
		}
		
		cb=(CheckBox) findViewById(R.id.cb_wheat);
		if(cb.isChecked())
		{
			allergies.add("WHEAT");
		}
		
		cb=(CheckBox) findViewById(R.id.cb_shellfish);
		if(cb.isChecked())
		{
			allergies.add("SHELLFISH");
		}
		
		userPreferencesPostRequest(userId,cuisines,category,allergies);
	}
	private void userPreferencesPostRequest(String userId, List<String> cuisines, String category, List<String> allergies) {

	    class UserPreferencesPostReqAsyncTask extends AsyncTask<Object, Void, String>{

	        @Override
	        protected String doInBackground(Object... params) {

	            String userId = (String)params[0];
	            List<?> cuisines = (List<?>)params[1];
	            String category = (String)params[2];
	            List<?> allergies = (List<?>)params[3];
	            System.out.println("*** doInBackground ** paramUsername " + userId + " paramPassword :" + category);

	            HttpClient httpClient = new DefaultHttpClient();

	            // In a POST request, we don't pass the values in the URL.
	            //Therefore we use only the web page URL as the parameter of the HttpPost argument
	            HttpPost httpPost = new HttpPost("http://54.215.152.32:8080/FoodRecommendationEngine/mobile/userpreferences");
	            httpPost.setHeader(HTTP.CONTENT_TYPE,
		                "application/x-www-form-urlencoded;charset=UTF-8");
	            // Because we are not passing values over the URL, we should have a mechanism to pass the values that can be
	            //uniquely separate by the other end.
	            //To achieve that we use BasicNameValuePair             
	            //Things we need to pass with the POST request
	            BasicNameValuePair usernameBasicNameValuePair = new BasicNameValuePair("userId", userId);
	            

	            // We add the content that we want to pass with the POST request to as name-value pairs
	            //Now we put those sending details to an ArrayList with type safe of NameValuePair
	            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
	            nameValuePairList.add(usernameBasicNameValuePair);
	            
	            for(int i=0;i<cuisines.size();i++)
	            {
	            	System.out.println("Cuisines:****"+(String) cuisines.get(i));
	            	nameValuePairList.add(new BasicNameValuePair("cuisines",(String) cuisines.get(i)));
	            }
	            
	            nameValuePairList.add(new BasicNameValuePair("category", category));
	            
	            for(int i=0;i<allergies.size();i++)
	            {
	            	System.out.println("Allergies:****"+(String) allergies.get(i));
	            	nameValuePairList.add(new BasicNameValuePair("allergies",(String) allergies.get(i)));
	            }

	            try {
	                // UrlEncodedFormEntity is an entity composed of a list of url-encoded pairs. 
	                //This is typically useful while sending an HTTP POST request. 
	                UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);

	                // setEntity() hands the entity (here it is urlEncodedFormEntity) to the request.
	                httpPost.setEntity(urlEncodedFormEntity);

	                try {
	                    // HttpResponse is an interface just like HttpPost.
	                    //Therefore we can't initialize them
	                    HttpResponse httpResponse = httpClient.execute(httpPost);

	                    // According to the JAVA API, InputStream constructor do nothing. 
	                    //So we can't initialize InputStream although it is not an interface
	                    InputStream inputStream = httpResponse.getEntity().getContent();

	                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

	                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

	                    StringBuilder stringBuilder = new StringBuilder();

	                    String bufferedStrChunk = null;

	                    while((bufferedStrChunk = bufferedReader.readLine()) != null){
	                        stringBuilder.append(bufferedStrChunk);
	                    }

	                    return stringBuilder.toString();

	                } catch (ClientProtocolException cpe) {
	                    System.out.println("First Exception caz of HttpResponese :" + cpe);
	                    cpe.printStackTrace();
	                } catch (IOException ioe) {
	                    System.out.println("Second Exception caz of HttpResponse :" + ioe);
	                    ioe.printStackTrace();
	                }

	            } catch (UnsupportedEncodingException uee) {
	                System.out.println("An Exception given because of UrlEncodedFormEntity argument :" + uee);
	                uee.printStackTrace();
	            }

	            return null;
	        }

	        @Override
	        protected void onPostExecute(String result) {
	            super.onPostExecute(result);
	            System.out.println("REsult*** "+result);
	            if(result != null && !result.isEmpty())
	            {	            		            
	            	try {
	            		//JSONObject obj = new JSONObject(result);
	            		MainActivity.pickFragment= true; 
	            		MainActivity.userPreferencesString = result;	            		
	            		Intent i = new Intent(getApplicationContext(),MainActivity.class);
	            		i.putExtra("foodItemsList", result);
		        		startActivity(i);     
	            		Toast.makeText(getApplicationContext(), "Updated", Toast.LENGTH_LONG).show();
	            	} catch (Exception ex) {
	            		Log.e("Error",ex.getMessage());
	                Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
	            	}	            	
	            }
	            else{
	                Toast.makeText(getApplicationContext(), "Invalid credentials", Toast.LENGTH_LONG).show();
	            }
	        }           
	    }

	    UserPreferencesPostReqAsyncTask sendPostReqAsyncTask = new UserPreferencesPostReqAsyncTask();
	    sendPostReqAsyncTask.execute(userId,cuisines,category,allergies);     
	}
}
